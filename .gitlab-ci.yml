
image: grpc/cxx:1.12.0 # default image runs debian:stretch

stages:
    - build
    - test
    - release

build_only:
  stage: build
  before_script:
    - apt-get update && apt-get install -y make cmake build-essential pkg-config libssl-dev libcurl4-openssl-dev
  script:
    - mkdir build
    - cd build
    - cmake -DBUILD_TESTING=OFF .. || exit 1
    - make || exit 1
  artifacts:
    paths:
      - build/bin/recc
      - test/data/hello.cpp

test_cover:
  stage: build
  coverage: '/^TOTAL.*\ (\d+\.*\d+\%)$/'
  dependencies: []
  before_script:
    - apt-get update && apt-get install -y make cmake build-essential pkg-config libssl-dev googletest libcurl4-openssl-dev
    - apt-get install -y python3 python3-pip wget
    - pip3 install gcovr pycobertura
  script:
    - mkdir build
    - cd build
    - export COV_FLGS="-g -O0 -fprofile-arcs -ftest-coverage"
    - cmake -DGTEST_SOURCE_ROOT=/usr/src/googletest -DCMAKE_CXX_FLAGS="${COV_FLGS}" -DCMAKE_EXE_LINKER_FLAGS="${COV_FLGS}" -DCMAKE_SHARED_LINKER_FLAGS="${COV_FLGS}" -DCMAKE_CXX_OUTPUT_EXTENSION_REPLACE="ON" .. || exit 1
    - make || exit 1
    - make test || exit 1
    - cd $CI_PROJECT_DIR
    - echo Generating gcov files
    - mkdir gcov && cd gcov
    - find "$CI_PROJECT_DIR/" -type f -name "*.gcno" -exec gcov --branch-counts --branch-probabilities --preserve-paths {} \;
    # remove gcov files that come from dependencies so that they don't show up on reports
    - find "$CI_PROJECT_DIR/gcov/" -type f -name "#*#build#gen*" -exec rm "{}" \;
    - find "$CI_PROJECT_DIR/gcov/" -type f -name "#*#third_party#*" -exec rm "{}" \;
    - find "$CI_PROJECT_DIR/gcov/" -type f ! -name "#*#src#*" -exec rm "{}" \;
    - cd "$CI_PROJECT_DIR"
    - mkdir -p  public/coverage
    - export LC_ALL=C.UTF-8 && export LANG=C.UTF-8
    - gcovr --xml-pretty -g -k > public/coverage/gcovr.xml
    - pycobertura show --format text --output public/coverage/report.txt -s "$CI_PROJECT_DIR" public/coverage/gcovr.xml
    - echo >> public/coverage/report.txt
    - pycobertura show --format html --output public/coverage/report.html -s "$CI_PROJECT_DIR" public/coverage/gcovr.xml
    - cat public/coverage/report.txt # show the textual report here, also used by GitLab to pick-up coverage %
  artifacts:
    paths:
      - public # name GitLab uses for Pages feature

# Check C++ code formatting using clang-format
# Since the GitLab CI API doesn't currently provide an MR Commit SHA so that we can
# run all the files modified in the current MR (Single Commit is doable) we just
# run clang-format for the diff between "empty tree magic SHA" and the current commit
# on all the C++ files (by extension: c,cc,cpp,cxx,h)
# Discussion on the "empty tree magic SHA": https://stackoverflow.com/questions/9765453/
# NOTE: git-clang-format runs only for files in the directories specified below
#       due to not having the option to exclude directories like "third_praty".
check_formatting:
    image: ubuntu:bionic
    stage: build
    before_script:
        - apt-get update && apt-get install -y clang-format-6.0 git-core
    script:
        - echo `which clang-format-6.0`
        - ln -s `which clang-format-6.0` /usr/bin/clang-format
        - cd "$CI_PROJECT_DIR"
        # The directories to check
        - export CLANG_FORMAT_DIRS="src/ test/"
        - export CLANG_FORMAT_SINCE_COMMIT="4b825dc642cb6eb9a060e54bf8d69288fbee4904"
        # No quotes around CLANG_FORMAT DIRS to explicitly split
        - linter_errors=$(git-clang-format-6.0 --commit "$CLANG_FORMAT_SINCE_COMMIT" -q --diff --style file --extensions c,cc,cpp,cxx,h  $CLANG_FORMAT_DIRS| grep -v --color=never "no modified files to format" || true)
        - echo "$linter_errors"
        - if [[ ! -z "$linter_errors" ]]; then echo "Detected formatting issues; please fix"; exit 1; else echo "Formatting is correct"; exit 0; fi

e2e:
    image: registry.gitlab.com/buildgrid/buildbox/buildbox-e2e:latest
    stage: test
    dependencies: []
    script:
        - RECC_SOURCE_ROOT=`pwd` end-to-end-test.sh
