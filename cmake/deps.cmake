# This function takes a variable name and a list of library names
# and mashes all of the linker flags for those libraries together

# It's useful for overriding variables generated by find_package, etc.
function(STATIC_LINK VAR_NAME DEPS EXTRA_FLAGS)
    set(${VAR_NAME} ${EXTRA_FLAGS})
    foreach(DEP ${DEPS})
        set(LIB_VAR "LIB_${DEP}")
        find_library(${LIB_VAR} ${DEP} REQUIRED)
        list(APPEND ${VAR_NAME} ${${LIB_VAR}})
    endforeach()
    set(${VAR_NAME} ${${VAR_NAME}} PARENT_SCOPE)
endfunction()

if(BUILD_STATIC)
    set(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
endif()

find_package(CURL REQUIRED)
if(BUILD_STATIC)
    set(DEPS
        "libcurl.a"
        "libssh2.a"
        "libgssapi_krb5.a"
        "libkrb5.a"
        "libk5crypto.a"
        "libcom_err.a"
        "libkrb5support.a")
    set(EXTRA_FLAGS "")
    static_link(CURL_LIBRARIES "${DEPS}" "${EXTRA_FLAGS}")
endif()

include_directories(${CURL_INCLUDE_DIR})

if(BUILD_STATIC)
    find_package(ZLIB REQUIRED)
endif()

find_package(OpenSSL REQUIRED)
set(OPENSSL_TARGET OpenSSL::Crypto)

find_package(Protobuf REQUIRED)
include_directories(${Protobuf_INCLUDE_DIRS})

find_package(gRPC)
if(gRPC_FOUND)
    set(GRPC_TARGET gRPC::grpc++)
    set(GRPC_CPP_PLUGIN $<TARGET_FILE:gRPC::grpc_cpp_plugin>)
else()
    find_package(PkgConfig REQUIRED)
    pkg_check_modules(gRPC REQUIRED IMPORTED_TARGET grpc++)
    set(GRPC_TARGET PkgConfig::gRPC)
    find_program(GRPC_CPP_PLUGIN grpc_cpp_plugin)
endif()
